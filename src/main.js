// Importaciones necesarias para integrar bootstra
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

// Importaciones necesarias para integrar awesome swiper
import VueAwesomeSwiper from "vue-awesome-swiper";
import "swiper/swiper-bundle.css";
Vue.use(VueAwesomeSwiper);
//Sweet Alert
import Vue from "vue";
import VueSweetalert2 from "vue-sweetalert2";

Vue.use(VueSweetalert2);
//Configuracion por default
import App from "./App.vue";
import router from "./router";
import store from "./store";
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
