import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [{
    path: "/",
    name: "Home",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/Bienvenida.vue"),
    beforeEnter: (to, from, next) => {
      if (localStorage.getItem("token")) {
        return next("/vista-administrador");
      }
      next();
    },
  },
  {
    path: "/bienvenida",
    name: "bienvenida",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/Bienvenida.vue"),
    beforeEnter: (to, from, next) => {
      if (localStorage.getItem("token")) {
        return next("/vista-administrador");
      }
      next();
    },
  },
  {
    path: "/bienvenido",
    name: "bienvenido",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/Bienvenida.vue"),
    beforeEnter: (to, from, next) => {
      if (localStorage.getItem("token")) {
        return next("/vista-administrador");
      }
      next();
    },
  },
  {
    path: "/guardar-ritual",
    name: "guardar-ritual",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/GuardarRitual.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/guardar-ritual/success",
    name: "guardar-ritual-success",
    component: () =>
      import(
        /* webpackChunkName: "about" */
        "../views/GuardarRitualSuccess.vue"
      ),
  },
  {
    path: "/programar-ritual",
    name: "ProgramarRitual",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/ProgramarRitual.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/vista-previa",
    name: "VistaPrevia",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/VistaPrevia.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/programar-ritual/success",
    name: "ProgramarRitual/success",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/ProgramarSuccess.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/registrar-cambios",
    name: "RegistrarCambios",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/RegistrarCambios.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/registrar-cambios/success",
    name: "RegistrarCambios/success",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/CambiosSuccess.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/proposito-vtr",
    name: "proposito-vtr",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/Objetivo.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/proposito",
    name: "proposito",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/Objetivo.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/test",
    name: "test",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/test.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/vtr-objetivo",
    name: "objetivo",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/Objetivo.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },

  {
    path: "/recuperar-contrasenia",
    name: "recuperar-contrasenia",
    component: () =>
      import(
        /* webpackChunkName: "about" */
        "../views/RecuperarContrasenia.vue"
      ),
  },
  {
    path: "/cambio-contrasenia",
    name: "cambio-contrasenia",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/CambioContrasenia.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/solicitud-contrasenia",
    name: "solicitud-contrasenia",
    component: () =>
      import(
        /* webpackChunkName: "about" */
        "../views/RecuperarContrasenia.vue"
      ),
  },
  {
    path: "/soporte",
    name: "soporte",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/Soporte.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/vista-administrador",
    name: "vista-administrador",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/VistaAdministrador.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/solicitar-video/success",
    name: "solicitar-video/success",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/SolicitarSuccess.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/Perfil",
    name: "perfil",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/Perfil.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/Perfil/success",
    name: "success",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/PerfilSuccess.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/Plan/downgrade",
    name: "plan-downgrade",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/PlanDowngrade.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/Plan/upgrade",
    name: "plan-upgrade",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/PlanUpgrade.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/ritual",
    name: "ritual",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/Ritual.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/historico",
    name: "historico",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/Historico.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/subir-video",
    name: "subir-video",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/SubirVideo.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/solicitar-video",
    name: "solicitar-video",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/SolicitarVideo.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/ingresar-solicitud",
    name: "ingresar-solicitud",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/IngresarSolicitud.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/crear-ritual",
    name: "crear-ritual",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/CrearRitual.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/planes",
    name: "planes",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/Planes.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/videoSuccess",
    name: "VideoSuccess",
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/VideoSuccess.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import( /* webpackChunkName: "about" */ "../views/About.vue"),
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem("token")) {
        return next("/bienvenida");
      }
      next();
    },
  },
];

const router = new VueRouter({
  routes,
});

export default router;